import React from 'react';
import {withRouter} from 'react-router-dom';
import '../../App.scss';
import TitleMain from '../TitleMain/TitleMain';
import TitleNews from '../TitleNews/TitleNews';
import NewsItem from '../NewsItem/NewsItem';
import NewsLink from '../NewsLink/NewsLink';

const Main = props => {
    const resultApi = props.data
    let itemsArr = null;
    const filterArr = resultApi && resultApi.filter((item, index) => {
        return index < 6;
    })
    props.location.pathname == '/' ? itemsArr = filterArr : itemsArr = resultApi;
    return (
        <div className="news">
            {
            props.location.pathname == '/' ? <TitleMain/> : 
            props.location.pathname == '/News' ? <TitleNews/> :
            null
            }
            <div className="news__list">
                {
                    itemsArr && itemsArr.map((item, index) => {
                        return (
                            <NewsItem
                                key={index}
                                title={item.title}
                                url={item.url}
                                source={item.source.name}
                                date={item.publishedAt}
                                obj={item}
                            />
                        )
                    })
                }
            </div>
            {props.location.pathname == '/' ? <NewsLink/> : null}
        </div>
    )
}

export default withRouter(Main);