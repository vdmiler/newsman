import React from 'react';
import '../../App.scss';
import img from '../../img/img.jpg'

const Contacts = () => {
    return (
        <div className="contacts">
            <div className="contacts__left">
                <a href="tel:+380963522919" className="contacts__tel">
                    +38 (096) 35 229 19
                </a>
                <p className="contacts__name">
                    Милер<br/> Вадим
                </p>
                <a href="vdmiler1985@gmail.com" className="contacts__email">
                    vdmiler1985@gmail.com
                </a>
                <p className="contacts__trade">
                    JavaScript разработчик
                </p>
                <p className="contacts__norm">
                    ES5, ES6, <span>React</span>
                </p>
            </div>
            <div className="contacts__right">
                <img src={img} alt="финиш" className="contacts__img"/>
            </div>
        </div>
    )
}

export default Contacts;