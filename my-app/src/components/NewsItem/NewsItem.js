import React from 'react';
import '../../App.scss';
import {withRouter} from 'react-router-dom';

const NewsItem = props => {
    const today = new Date(props.date);
    const getZero = date => date < 10 ? '0' + date : date;
    const month = getZero(today.getMonth() + 1);
    const day = getZero(today.getDate());
    let newUrl = props.title.replace(/\s/g, '_');
    return (
        <div className="news__item" onClick={() => {props.history.push('/News/' + newUrl, [props.obj])}}>
            <h3 className="news__preview">
                {props.title}
            </h3>
            <div className="news__sign">
                <a href={props.url} className="news__source">
                    {props.source}
                </a>
                <p className="news__date">
                    <span>{month}</span>
                    {day}
                </p>
            </div>
        </div>
    )
}

export default withRouter(NewsItem);