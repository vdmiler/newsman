import React from 'react';
import '../../App.scss';

const TitleMain = () => {
    return (
        <h1 className="news__title news__title_main">
            Всегда свежие <span>новости</span>
        </h1>
    )
}

export default TitleMain;