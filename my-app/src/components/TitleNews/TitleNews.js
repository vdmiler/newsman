import React from 'react';
import '../../App.scss';

const TitleNews = () => {
    return (
        <h1 className="news__title news__title_news">
            Быть<br /> в курсе <span>событий</span>
        </h1>
    )
}

export default TitleNews;