import React from 'react';
import '../../App.scss';
import {NavLink} from 'react-router-dom';

const NewsLink = () => {
    return (
        <NavLink exact to="/News" className="news__link">
            Быть в курсе событий
        </NavLink>
    )
}

export default NewsLink;