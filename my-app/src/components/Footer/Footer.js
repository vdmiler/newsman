import React from 'react';
import '../../App.scss';

const Footer = () => {
    return (
        <footer className="footer" id="footer">
            <div className="container">
                <div className="ft-content">
                    <div className="ft-content__left">
                       <p className="ft-content__strong">
                            Новостник
                        </p>
                        <p className="ft-content__light">
                            Single Page Application
                        </p>
                    </div>
                    <div className="ft-content__center">
                        <p className="ft-content__light">
                            Дипломный проект
                        </p>
                    </div>
                    <div className="ft-content__right">
                        <p className="ft-content__light">
                            Made by
                        </p>
                        <p className="ft-content__strong">
                            Вадим Милер
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;