import React, {useEffect, useState} from 'react';
import './App.scss';
import {Route} from 'react-router-dom';
import Header from './components/Header/Header';
import Main from './components/Main/Main';
import NewsDetail from './components/NewsDetail/NewsDetail';
import Contacts from './components/Contacts/Contacts';
import Footer from './components/Footer/Footer';

const useFetch = () => {
  const [data, updateData] = useState(null);
  const requestUrl = 'https://newsapi.org/v2/top-headlines?country=us';
  useEffect(() => {
      fetch(requestUrl, {
          headers: {
              Authorization: 'Bearer c6845b90dfa847d1b4b5c25cc4923285'
          }
      })
          .then(response => response.json())
          .then(json => json && updateData(json.articles)) 
              
  }, [])
  return data;
}

function App() {
  const result = useFetch();
  return (
    <div className="App">
      <Header/>
      <div className="content">
          <div className="container">
              <Route
                  path="/"
                  exact
                  render={()=><Main data={result}/>}
              />
              <Route
                  path="/News"
                  exact
                  render={()=><Main data={result}/>}
              />
              <Route
                  path="/Contacts"
                  exact
                  component={Contacts}
              />
              <Route
                  path="/News/:name"
                  exact
                  component={NewsDetail}
              />
          </div>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
